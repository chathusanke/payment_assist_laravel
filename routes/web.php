<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

/* Route::get('/', function () {
  return view('welcome');
  }); */


Route::get('/', 'RegistrationRequestController@create');
Route::get('registrationrequest', 'RegistrationRequestController@create');
Route::post('registrationrequest/send', 'RegistrationRequestController@store');
Route::get('sendbasicemail', 'MailController@basic_email');
Route::get('sendhtmlemail', 'MailController@html_email');
Route::get('sendattachmentemail', 'MailController@attachment_email');
Route::get('invitation', 'RegistrationRequestController@validate_url');
Route::get('expired', 'Controller@empty_page');
/* Route::group([
  'middleware' => 'admin.permission:allow,administrator,editor',
  ], function ($router) {

  $router->get('company/create', 'CompanyController@create');
  }); */
Route::get('company/create', 'CompanyController@create');
Route::get('company/test', 'CompanyController@test');
Route::post('company/save', 'CompanyController@store');
Route::group([
    'middleware' => 'admin.permission:allow,administrator,editor',
        ], function ($router) {

    $router->get('/testing123', function() {
        return 'success test123';
    });
});

Route::get('quotation/create', 'QuotationController@create')->middleware('test');
Route::post('quotation/save', 'QuotationController@save');
Route::post('quotation/edit', 'QuotationController@backtoform');
Route::get('quotaion/send', 'QuotationController@pay');


//Testing database connection
Route::get('testdb', function() {
    try {
        DB::connection()->getPdo();
        echo "db connection success";
    } catch (Exception $e) {
        die("Could not connect to the database." . $e->getMessage());
    }
});
//$router->resource('/users', UserController::class);
//Route::get('/user', 'Admin\UserController@index');
