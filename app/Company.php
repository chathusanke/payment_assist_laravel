<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model {

    protected $fillable = ['full_name', 'address', 'contact', 'email', 'website', 'business_type'];

}
