<?php

namespace App\Http\Controllers;

//namespace App\Admin\Controllers;

use Illuminate\Http\Request;
use App\Company;
use App\BusinessType;
use Encore\Admin\Auth\Permission;

/* use Encore\Admin\Form;
  use Encore\Admin\Facades\Admin; */

class CompanyController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /* public function test() {
      $grid = Admin::form(Company::class, function(Form $grid) {

      // Displays the record id
      $form->display('id', 'ID');

      // Add an input box of type text
      $form->text('title', 'Movie title');

      $directors = [
      'John' => 1,
      'Smith' => 2,
      'Kate' => 3,
      ];

      $form->select('director', 'Director')->options($directors);

      // Add textarea for the describe field
      $form->textarea('describe', 'Describe');

      // Number input
      $form->number('rate', 'Rate');

      // Add a switch field
      $form->switch('released', 'Released?');

      // Add a date and time selection box
      $form->dateTime('release_at', 'release time');

      // Display two time column
      $form->display('created_at', 'Created time');
      $form->display('updated_at', 'Updated time');
      });
      } */

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        Permission::check('company');
        $business_type = BusinessType::all();
        return view('company.create')->with('types', $business_type);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //die($request);
        $this->validate($request, [
            'full_name' => 'required',
            'address' => 'required',
            'contact' => 'required',
            'email' => 'required',
            'website' => 'required',
        ]);
        try {

            if ($request->input('type') != null) {
                $type = BusinessType::where('type', trim($request->input('type')))->first();
                if ($type) {
                    $request['business_type'] = $type->id;
                } else {
                    $business_type = new BusinessType;
                    $business_type->type = $request->input('type');
                    $business_type->save();
                    $request['business_type'] = $business_type->id;
                }
            }
            $company = new Company;
            $company->create($request->all());
            return redirect('/')->with('success', 'Company details saved.Login link sent to primary and secondary user emails.');
        } catch (Exception $ex) {
            return $ex->getmessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
