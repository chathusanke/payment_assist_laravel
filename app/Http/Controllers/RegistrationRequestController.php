<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\registrationrequest;
use Mail;
use UrlSigner;

class RegistrationRequestController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('registrationRequest.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, ['email' => 'required'
        ]);
        //die($request['email']);
        $reg_reqest = registrationrequest::where('email', $request['email'])->first();
        if ($reg_reqest) {
            //$errors=array('s','c');
            return redirect('registrationrequest')->with('warning', 'Sorry! Given email address is already registered');
        } else {
            $reg_reqest = new registrationrequest;
            $reg_reqest->email = $request['email'];
            $reg_reqest->save();
            app('App\Http\Controllers\MailController')->send_invitataion($request['email'], $reg_reqest->id);
            $reg_reqest->send_status = 1;
            $reg_reqest->save();
            return redirect('registrationrequest')->with('success', 'Thank you!Registration link will sent to your email.');
        }
        ///  print_r($reg_reqest);
    }

    //validate customer by invitation linkpager

    public function validate_url(Request $request) {

        $reg_request = registrationrequest::find($request['id']);
        if (!$reg_request->click_status) {
            $reg_request->click_status = 1;
            $reg_request->save();
            $data = array(
                'user_id' => $request['id'],
                'email' => $reg_request->email
            );
            return redirect('company/create')->with('data', $data);
        } else {
            return redirect('expired')->with('success', 'link expired');
        }
        //die($reg_request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
