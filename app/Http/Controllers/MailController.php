<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Mail;
use UrlSigner;
use Illuminate\Routing\UrlGenerator;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class MailController extends Controller {
   public function basic_email(){
      $data = array('name'=>"Virat Gandhi");

      Mail::send(['text'=>'mail'], $data, function($message) {
         $message->to('chathusanke@rukizone.com', 'Tutorials Point')->subject
            ('Laravel Basic Testing Mail');
         $message->from('chathusanker5@gmail.com','Virat Gandhi');
      });
      echo "Basic Email Sent. Check your inbox.";
   }
   public function html_email(){$this->send_invitataion();die();
     $url=UrlSigner::sign('http://localhost:8000/');
      $data = array('name'=>"Redfluence pay",'url'=>$url);
      Mail::send('mail.request', $data, function($message) {
         $message->to('chathusanke@rukizone.com', 'Tutorials Point')->subject
            ('Laravel HTML Testing Mail');
         $message->from('xyz@gmail.com','Virat Gandhi');
      });
      echo "HTML Email Sent. Check your inbox.";
   }
   private function send_email($receiver,$message){//die($receiver);
     Mail::send('mail.request', $message, function($email)use($receiver) {//die($receiver);
               $email->to($receiver, 'kamal')->subject('Registration request');
        $email->from('chathusanker5@gmail.com','Redfluence pay');
     });
   }
   public function send_invitataion($receiver='chathusanke@rukizone.com',$request_id='2'){
     $url=UrlSigner::sign('http://localhost:8000/invitation?id='.$request_id);
     $message=array('url'=>$url);
     $this->send_email($receiver,$message);
   }
   public function attachment_email(){
      $data = array('name'=>"Virat Gandhi");
      Mail::send('mail', $data, function($message) {
         $message->to('abc@gmail.com', 'Tutorials Point')->subject
            ('Laravel Testing Mail with Attachment');
         $message->attach('C:\laravel-master\laravel\public\uploads\image.png');
         $message->attach('C:\laravel-master\laravel\public\uploads\test.txt');
         $message->from('xyz@gmail.com','Virat Gandhi');
      });
      echo "Email Sent with attachment. Check your inbox.";
   }
}
