@php $user_id='user_id'; 
$email='default email'; @endphp
@if(session('data'))
@foreach(session('data') as $k => $v)

@if($k=='user_id')
@php $user_id=$v; @endphp
@else
@php $email=$v; @endphp
@endif
@endforeach

@else
<!--script>window.location = "/";</script-->
@endif
@extends('layout.app')
@section('content')
<h2 class="text-center">Customer Registration Form </h2>
<form class="form-horizontal" method="post" action="save">
    <h3>Genaral Information</h3>
    <div class="form-group">
        <label for="full_name" class="col-sm-2 control-label">Company Name</label>
        <div class="col-sm-10">
            <input type="text" name="full_name" class="form-control" id="full_name" placeholder="Full Name of Company">
        </div>
    </div>
    <div class="form-group">
        <label for="address" class="col-sm-2 control-label">Company Address</label>
        <div class="col-sm-10">
            <input type="text" name="address" class="form-control" id="address" placeholder="Company Address">
        </div>
    </div>
    <div class="form-group">
        <label for="contact" class="col-sm-2 control-label">Contact Number</label>
        <div class="col-sm-10">
            <input type="text" name="contact" class="form-control" id="contact" placeholder="Contact Number">
        </div>
    </div>
    <div class="form-group">
        <label for="email" class="col-sm-2 control-label">Email</label>
        <div class="col-sm-10">
            <input type="email" name="email" class="form-control" id="email" value="{{$email}}" readonly>
        </div>
    </div>
    <div class="form-group">
        <label for="website" class="col-sm-2 control-label">Website</label>
        <div class="col-sm-10">
            <input type="text" name="website" class="form-control" id="website" placeholder="Website">
        </div>
    </div>
    <div class="form-group" id="business_type_wrapper">
        <label for="business_type" class="col-sm-2 control-label">Business Type</label>
        <div class="col-sm-10">
            @if(count($types)>0)
            @foreach($types as $type)
            <label class="radio-inline"><input type="radio" name="business_type"  id="business_type" value="{{$type->id}}">{{$type->type}}</label>
            @endforeach
            @endif
            <label class="radio-inline"><input type="radio" name="business_type"  id="business_type_other" value="0">Other</label>
        </div>

    </div>

    <hr>

    <h3>Primary Contact Person</h3>
    <div class="form-group">
        <label for="fullname_primarey" class="col-sm-2 control-label">Full name</label>
        <div class="col-sm-10">
            <input type="text" name="fullname_primarey" class="form-control" id="fullname_primarey" placeholder="Full Name of primary contact">
        </div>
    </div>
    <div class="form-group">
        <label for="designation_primarey" class="col-sm-2 control-label">Designation</label>
        <div class="col-sm-10">
            <input type="text" name="designation_primarey" class="form-control" id="designation_primarey" placeholder="Designation of primary contact">
        </div>
    </div>
    <div class="form-group">
        <label for="contact_primarey" class="col-sm-2 control-label">Contact Number</label>
        <div class="col-sm-10">
            <input type="text" name="contact_primarey" class="form-control" id="contact_primarey" placeholder="Contact of primary contact">
        </div>
    </div>
    <div class="form-group">
        <label for="email_primarey" class="col-sm-2 control-label">Email</label>
        <div class="col-sm-10">
            <input type="text" name="email_primarey" class="form-control" id="email_primarey" placeholder="Email of primary contact">
        </div>
    </div>
    <hr>
    <h3>Secondary Contact Person</h3>
    <div id='secondary_contact'>
        <div class="form-group">
            <label for="fullname_secondary01" class="col-sm-2 control-label">Full name</label>
            <div class="col-sm-10">
                <input type="text" name="fullname_secondary01" class="form-control" id="fullname_secondary01" placeholder="Full Name of secondary contact">
            </div>
        </div>
        <div class="form-group">
            <label for="designation_secondary01" class="col-sm-2 control-label">Designation</label>
            <div class="col-sm-10">
                <input type="text" name="designation_secondary01" class="form-control" id="designation_secondary01" placeholder="Designation of secondary contact">
            </div>
        </div>
        <div class="form-group">
            <label for="contact_secondary01" class="col-sm-2 control-label">Contact Number</label>
            <div class="col-sm-10">
                <input type="text" name="contact_secondary01" class="form-control" id="contact_secondary01" placeholder="Contact of secondary contact">
            </div>
        </div>
        <div class="form-group">
            <label for="email_secondary01" class="col-sm-2 control-label">Email</label>
            <div class="col-sm-10">
                <input type="text" name="email_secondary01" class="form-control" id="email_secondary01" placeholder="Email of secondary contact">
            </div>
        </div>

    </div>
    <center><button type="button" id="add_section" class="btn btn-outline-primary btn-rounded btn-lg">+</button></center>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">Sign in</button>
        </div>
    </div>
    {{ csrf_field() }}
</form>
@endsection
@section('script')
<script>
    $(function () {
        secondaryCount = 1;
        $('#add_section').click(function () {
            addSecondary();
        })
        $('input:radio[name="business_type"]').change(function () {
            selected_value = $("input[name='business_type']:checked").val();
            if (selected_value == 0) {
                addNewBusinessType();
            } else {
                removeBusinessType();
            }
        });
        /*$('input:radio[id="business_type_other"]').change(
         function () {
         if ($(this).is(':checked') && $(this).val() == '0') {
         addNewBusinessType();
         } else {
         removeBusinessType();
         }
         });*/
    })
    function addSecondary() {
        $('#secondary_contact').append(`
        <hr>
            <div class="form-group">
            <label for="fullname_secondary` + secondaryCount + `" class="col-sm-2 control-label">Full name</label>
            <div class="col-sm-10">
                <input type="text" name="fullname_secondary` + secondaryCount + `" class="form-control" id="fullname_secondary` + secondaryCount + `"  placeholder="Full Name of secondary contact">
            </div>
        </div>
        <div class="form-group">
            <label for="designation_secondary` + secondaryCount + `"  class="col-sm-2 control-label">Designation</label>
            <div class="col-sm-10">
                <input type="text" name="designation_secondary` + secondaryCount + `"  class="form-control" id="designation_secondary` + secondaryCount + `"  placeholder="Designation of secondary contact">
            </div>
        </div>
        <div class="form-group">
            <label for="contact_secondary` + secondaryCount + `"  class="col-sm-2 control-label">Contact Number</label>
            <div class="col-sm-10">
                <input type="text" name="contact_secondary` + secondaryCount + `"  class="form-control" id="contact_secondary` + secondaryCount + `"  placeholder="Contact of secondary contact">
            </div>
        </div>
        <div class="form-group">
            <label for="email_secondary` + secondaryCount + `"  class="col-sm-2 control-label">Email</label>
            <div class="col-sm-10">
                <input type="text" name="email_secondary` + secondaryCount + `"  class="form-control" id="email_secondary` + secondaryCount + `"  placeholder="Email of secondary contact">
            </div>
        </div>

    </div>
        `);
        secondaryCount++;
    }
    function addNewBusinessType() {
        $("#business_type_wrapper").append(`
            <div class="col-sm-10 col-sm-push-2" id="new_type_text">
            <input type="text" name="type" class="form-control" id="type" placeholder="If others please specify">
            </div>
        `);
    }
    function removeBusinessType() {
        $('#new_type_text').remove();
    }
</script>
@endsection

