@php
$company_name='ABC Company pvt ltd';
$company_address='No 14,Wattaranthenna road,Kandy';
$company_phone='0112569865';
@endphp
@extends('layout.app')
@section('content')
<h2 class="text-center">Quotation Create Form </h2>
<form class="form-horizontal" method="post" action="save">
    {{ csrf_field() }}
    <div class="col-sm-6">
        <div class="form-group">
            <label for="Company_name" class="col-sm-2 control-label">Company Name</label>
            <div class="col-sm-10">
                <input type="text" name="Company_name" class="form-control" id="Company_name" placeholder="Company name" value="{{$company_name}}" readonly>
            </div>
        </div>
        <div class="form-group">
            <label for="company_address" class="col-sm-2 control-label">Company address</label>
            <div class="col-sm-10">
                <input type="text" name="company_address" class="form-control" id="company_address" placeholder="Company address" value="{{$company_address}}" readonly>
            </div>
        </div>
        <div class="form-group">
            <label for="company_phone" class="col-sm-2 control-label">Company phone</label>
            <div class="col-sm-10">
                <input type="text" name="company_phone" class="form-control" id="company_phone" placeholder="Company phone" value="{{$company_phone}}" readonly>
            </div>
        </div>
        <div class="form-group">
            <label for="date" class="col-sm-2 control-label">Date</label>
            <div class="col-sm-10">
                <input type="date" name="date" class="form-control" id="date" placeholder="Company address" value="{{date('Y-m-d H:i:s')}}" >
            </div>
        </div>
        <div class="form-group">
            <label for="tax" class="col-sm-2 control-label">Tax rate</label>
            <div class="col-sm-10">
                <input type="number" name="tax" class="form-control" id="tax" placeholder="Company tax rate" value="2.5" >
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label for="client_name" class="col-sm-2 control-label">Client Name</label>
            <div class="col-sm-10">
                <input type="text" name="client_name" class="form-control" id="client_name" placeholder="Client Name" value="" >
            </div>
        </div>
        <div class="form-group">
            <label for="client_address" class="col-sm-2 control-label">Client Address</label>
            <div class="col-sm-10">
                <input type="text" name="client_address" class="form-control" id="client_address" placeholder="Client Address" value="" >
            </div>
        </div>
    </div>
    <div class="col-sm-12">
        <div id="item-wrapper">
            <h3>Add items</h3>
            <div class="form-group" id='item_1'>
                <label for="date" class="col-sm-3 ">
                    <input type="text" name="item1['name']" class="form-control" id="item_name_1" placeholder="Item name" value="" >

                </label>
                <label for="date" class="col-sm-5">
                    <input type="text" name="item1['desc']" class="form-control" id="item_name_1" placeholder="Item description" value="" >

                </label>
                <div class="col-sm-2">
                    <input type="number" name="item1['price']" class="form-control item_price_input" id="item_price_1" placeholder="Price" value="" >
                </div>
                <div class="col-sm-2">
                    <button type="button" id="remove_item" class="btn-remove-item btn btn-outline-danger btn-rounded-danger btn-sm" value="1"> - </button>

                </div>
            </div>

        </div>
        <center><button type="button" id="add_item" class="btn btn-outline-primary btn-rounded-primary btn-lg">+</button></center>
        <div id="final_display">
            <div class="form-group">
                <label for="tax_val" class="col-sm-2 control-label">Tax</label>
                <div class="col-sm-10">
                    <input type="text" name="tax" class="control-label txt-borderless" id="tax_val">
                </div>
            </div>
            <div class="form-group">
                <label for="total" class="col-sm-2 control-label">Total</label>
                <div class="col-sm-10">

                    <input type="text" name="total" class="control-label txt-borderless" id="total_price">
                </div>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" name="submit" class="btn btn-success right">Create</button>
        </div>
    </div>
</form>
@endsection
@section('script')
<script>
    $(function () {
        //tax = parseFloat($('#tax').val());
        sum = 0;
        itemCount = 1;
        $('#add_item').click(function () {
            addformItem();
            UpdateTotalAdd();

            //addItem($('#item_price_' + (itemCount - 1)).val());
        });

        $('#item-wrapper').on('click', '.btn-remove-item', function () {
            UpdateTotalSub($('#item_price_' + $(this).val()).val());
            //subtractItem($('#item_price_' + $(this).val()).val());
            $('#item_' + $(this).val()).remove();
        });
    });
    function addformItem() {
        itemCount++;
        $('#item-wrapper').append(`
           <div class="form-group" id='item_` + itemCount + `'>
            <label for="date" class="col-sm-3">
                <input type="text" name="item` + itemCount + `['name']" class="form-control" id="item` + itemCount + `['name']" placeholder="Item name" value="" >

            </label>
            <label for="date" class="col-sm-5">
                <input type="text" name="item` + itemCount + `['desc']" class="form-control" id="item` + itemCount + `['desc']" placeholder="Item description" value="" >

            </label>
            <div class="col-sm-2">
                <input type="number" name="item` + itemCount + `['price']" class="form-control item_price_input" id="item` + itemCount + `['price']" placeholder="Price" value="" >
            </div>
            <div class="col-sm-2">
                <button type="button" id="remove_item" class="btn-remove-item btn btn-outline-danger btn-rounded-danger btn-sm" value="` + itemCount + `""> - </button>

            </div>
        </div>
        `);
    }

    /*function addItem(numberAdd) {

     sum += parseFloat(numberAdd);
     $('#total_price').text(sum);
     $('#tax_val').text(sum * tax);
     }
     function subtractItem(numberSub) {
     sum -= parseFloat(numberSub);
     $('#total_price').text(sum);
     $('#tax_val').text((sum * tax / 100));
     }-*/

    function UpdateTotalAdd() {
        var total = 0;
        var $changeInputs = $('input.item_price_input');
        $changeInputs.each(function (idx, el) {
            total += Number($(el).val());
        });

        var tax = UpdateTax(total, parseFloat($('#tax').val()))
        $('#total_price').val(total + tax);

    }
    function UpdateTotalSub(numberToSub) {
        var total = 0;
        var $changeInputs = $('input.item_price_input');
        $changeInputs.each(function (idx, el) {
            total += Number($(el).val());

        });
        var final = total - numberToSub;
        //$('#total_price').text(final);
        var tax = UpdateTax(total, parseFloat($('#tax').val()))
        $('#total_price').val(total + tax);

    }
    function UpdateTax(total, rate) {
        var temp = (rate / 100) * total;
        $('#tax_val').val(temp);
        return temp;
    }
</script>
@endsection