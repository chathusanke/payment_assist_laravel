@php
//var_dump($data);
//die();
@endphp
@extends('layout.app')
@section('style')
<link href="{{ asset('css/quotation.css') }}" rel="stylesheet">
@endsection
@section('content')
<div class="invoice-box">
    <table cellpadding="0" cellspacing="0">
        <tr class="top">
            <td colspan="2">
                <table>
                    <tr>
                        <td class="title">
                            <img src="https://www.sparksuite.com/images/logo.png" style="width:100%; max-width:300px;">
                        </td>

                        <td>
                            Invoice #: 123<br>
                            Created: {{$data['date']}}<br>
                            Due: February 1, 2015
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr class="information">
            <td colspan="2">
                <table>
                    <tr>
                        <td>
                            {{$data['client_name']}}<br>
                            {{$data['client_address']}}<br>

                        </td>

                        <td>
                            {{$data['Company_name']}}<br>
                            Chathusanke Ratnayake<br>
                            www.companyweb.com
                        </td>
                    </tr>
                </table>
            </td>
        </tr>



        <tr class="heading">
            <td>
                Item
            </td>

            <td>
                Price
            </td>
        </tr>

        @php
        foreach($data as $key=>$val){
        if (strpos($key, 'item') !== false){
        @endphp
        <tr class="item">
            @php
            $counter = 0;
            foreach($val as $key=>$item){
            if (strpos($key, 'name') !== false) continue;
            echo '<td>'.$item.'</td>';
            }
            @endphp
        <tr class="item">
            @php
            }

            }
            @endphp


        <tr class="">
            <td>Tax</td>

            <td>
                {{$data['tax']}}
            </td>
        </tr>
        <tr class="total">
            <td>Total</td>

            <td>
                {{$data['total']}}
            </td>
        </tr>
    </table>
</div>
<div class="row">
    <div class="col-sm-6">
        <a class="btn btn-success" href="">Send to client</a>
    </div>
    <div class="col-sm-6">
        <a class="btn btn-success" href="{{URL::to('/')}}/quotation/edit">Edit</a>
    </div>
</div>

@endsection