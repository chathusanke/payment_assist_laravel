<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Redfluence-Pay</title>
        <link rel="stylesheet" href="/css/app.css">
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
        @yield('style')
    </head>
    <body>
        @include('inc.navbar')
        <div class="container">
            @include('inc.messages')
            @yield('content')
        </div>
        <footer><center>Copyright &copy; {{date('Y')}} Redfluence-Pay</center></footer>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        @yield('script')
    </body>
</html>
