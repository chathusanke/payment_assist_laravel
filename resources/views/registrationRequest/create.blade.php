@extends('layout.app')
@section('content')
<form class="form-horizontal" method="post" action="registrationrequest/send">
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
    <div class="col-sm-10">
      <input type="email" name="email" class="form-control" id="inputEmail3" placeholder="Email">
    </div>
  </div>
  <div class="form-group">
   <div class="col-sm-offset-2 col-sm-10">
     <button type="submit" class="btn btn-default">Sign in</button>
   </div>
 </div>
 {{ csrf_field() }}
</form>
@endsection
